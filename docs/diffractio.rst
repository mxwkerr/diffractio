diffractio package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   diffractio.add_ons

Submodules
----------

diffractio.scalar\_fields\_X module
-----------------------------------

.. automodule:: diffractio.scalar_fields_X
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_fields\_XY module
------------------------------------

.. automodule:: diffractio.scalar_fields_XY
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_fields\_XYZ module
-------------------------------------

.. automodule:: diffractio.scalar_fields_XYZ
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_fields\_XZ module
------------------------------------

.. automodule:: diffractio.scalar_fields_XZ
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_masks\_X module
----------------------------------

.. automodule:: diffractio.scalar_masks_X
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_masks\_XY module
-----------------------------------

.. automodule:: diffractio.scalar_masks_XY
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_masks\_XYZ module
------------------------------------

.. automodule:: diffractio.scalar_masks_XYZ
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_masks\_XZ module
-----------------------------------

.. automodule:: diffractio.scalar_masks_XZ
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_sources\_X module
------------------------------------

.. automodule:: diffractio.scalar_sources_X
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.scalar\_sources\_XY module
-------------------------------------

.. automodule:: diffractio.scalar_sources_XY
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.utils\_common module
-------------------------------

.. automodule:: diffractio.utils_common
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.utils\_drawing module
--------------------------------

.. automodule:: diffractio.utils_drawing
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.utils\_math module
-----------------------------

.. automodule:: diffractio.utils_math
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.utils\_multiprocessing module
----------------------------------------

.. automodule:: diffractio.utils_multiprocessing
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.utils\_optics module
-------------------------------

.. automodule:: diffractio.utils_optics
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.utils\_slicer module
-------------------------------

.. automodule:: diffractio.utils_slicer
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.utils\_tests module
------------------------------

.. automodule:: diffractio.utils_tests
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.vector\_paraxial\_fields\_XY module
----------------------------------------------

.. automodule:: diffractio.vector_paraxial_fields_XY
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.vector\_paraxial\_masks\_XY module
---------------------------------------------

.. automodule:: diffractio.vector_paraxial_masks_XY
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.vector\_paraxial\_sources\_XY module
-----------------------------------------------

.. automodule:: diffractio.vector_paraxial_sources_XY
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: diffractio
   :members:
   :undoc-members:
   :show-inheritance:
