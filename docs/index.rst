Welcome to Diffractio: Python diffraction and interference's  documentation!
===================================================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:
   :numbered:
   :glob:




   readme
   installation
   usage
   modules
   tutorials
   examples
   contributing
   authors
   results
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
