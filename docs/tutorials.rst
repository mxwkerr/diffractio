Tutorials
=============

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:

   source/tutorial/scalar_X/tutorial.rst
   source/tutorial/scalar_XZ/tutorial.rst
   source/tutorial/scalar_XY/tutorial.rst
   source/tutorial/scalar_XYZ/tutorial.rst
   source/tutorial/vector_paraxial_XY/tutorial.rst
   source/tutorial/drawing/tutorial.rst
