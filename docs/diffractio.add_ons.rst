diffractio.add\_ons package
===========================

Submodules
----------

diffractio.add\_ons.angular\_lens module
----------------------------------------

.. automodule:: diffractio.add_ons.angular_lens
   :members:
   :undoc-members:
   :show-inheritance:

diffractio.add\_ons.gerchberg\_saxton module
--------------------------------------------

.. automodule:: diffractio.add_ons.gerchberg_saxton
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: diffractio.add_ons
   :members:
   :undoc-members:
   :show-inheritance:
