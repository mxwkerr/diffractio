Functioning
=============

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:

   artifacts_bpm.ipynb
   multiprocessing.ipynb
   power_z_propagation.ipynb
   surfaces.ipynb
   developing_new_functions.ipynb