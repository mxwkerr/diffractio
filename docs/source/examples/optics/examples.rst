Optical examples
==========================

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:


   gauss_beam.ipynb
   reflection_refraction.ipynb
   diffraction_slit.ipynb
   arago_point.ipynb
   diffraction_objects.ipynb
   scattering_small_cylinders.ipynb
   fresnel_biprism.ipynb
   lenses.ipynb
   fresnel_lens.ipynb
   talbot_effect.ipynb
   variable_refraction_index.ipynb
   vector_double_slit_experiment.ipynb
   VRS_paper.ipynb
