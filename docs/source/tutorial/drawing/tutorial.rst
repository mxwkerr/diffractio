Some info about drawing and videos
=======================================

.. toctree::
   :maxdepth: 4
   :numbered:
   :glob:

   creating_video.ipynb
   use_interact_visualize_profiles.ipynb
   interactive_in_diffractio.ipynb
   external_qt.ipynb
