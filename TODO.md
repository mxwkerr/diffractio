# Todo

## Bugs

-   utils_math has several normalize functions

## alpha

-   Check that RS is working properly of XYZ (multiprocessing)
-   XZ and  XYZ: function for interactive drawing I(x,y;z)

Algorithms:

-   Implement PWD
-   Implement WPM
-   Implement VPM
-   Implement new BPM
-   Implemement VRS - working in development

Masks:

-   XYZ másk - develop more masks
-   -   vectorial - implement complex XY masks (from SLM for example and g.l.)

## others

-   tests for utils
