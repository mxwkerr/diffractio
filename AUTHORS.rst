===========
Credits
===========

Development Lead
---------------------------

* Luis Miguel Sanchez Brea <optbrea@ucm.es>


    **Universidad Complutense de Madrid**,
    Faculty of Physical Sciences,
    Department of Optics
    Plaza de las ciencias 1,
    ES-28040 Madrid (Spain)

Authors
----------------------------

* Ángela Soria García (2021)

    * XY optimizations
    * XY arrays of masks

Contributors
--------------

* Manuel Rapariz López-Neira (2019)

    * XY masks (hiperellipse and sinusoidal_slit)

* Joaquín Andrés Porras (2021)

    * XY masks (angular_aperture, edge_series, slit_series)
